#!/usr/bin/env python3
import socket
import time
from pyparrot.Minidrone import Mambo

mamboAddr = "F0:03:8C:2F:5E:4D"
mambo = Mambo(mamboAddr, use_wifi=True)

def main():
    print("Trying to connect")
    success = mambo.connect(num_retries=3)
    print("Connected: %s" % success)

    if (success):
        host = '127.0.0.1'
        port = 5000
        
        parSocket = socket.socket()
        parSocket.connect((host,port))

        print("Loading params")
        mambo.smart_sleep(2)
        mambo.ask_for_state_update()
        mambo.smart_sleep(2)        

        t_start = time.time()        
                
        while(mambo.sensors.flying_state != "emergency"):
            mambo.ask_for_state_update()
            t_delta = time.time() - t_start        
            
            batt = mambo.sensors.battery
            state = mambo.sensors.flying_state
            vx = mambo.sensors.speed_x
            vy = mambo.sensors.speed_y
            vz = mambo.sensors.speed_z
            v_ts = mambo.sensors.speed_ts
            alt = mambo.sensors.altitude
            alt_ts = mambo.sensors.altitude_ts
            qw = mambo.sensors.quaternion_w
            qx = mambo.sensors.quaternion_x
            qy = mambo.sensors.quaternion_y
            qz = mambo.sensors.quaternion_z
            q_ts = mambo.sensors.quaternion_ts
            roll, pitch, yaw = mambo.sensors.quaternion_to_euler_angle(qw, qx, qy, qz)            

            print("Time: %f" % t_delta)
            print("Battery: %f" % batt)
            # print("Flying state: %s" % state)
            # print("Velocity: x=%f, y=%f, z=%f, ts=%f" % (vx, vy, vz, v_ts))
            # print("Altitude: h=%f, ts=%f" % (alt, alt_ts))
            # print("Quaternion: w=%f, x=%f, y=%f, z=%f, ts=%f" % (qw, qx, qy, qz, q_ts))
            # print("Euler: r=%f, p=%f, y=%f" % (roll, pitch, yaw))
            # print("=================================")
            
            message = "%f#%s#%f#%f#%f#%f#%f#%f#%f#%f#%f" % (batt, state, vx, vy, vz, alt, qw, qx, qy, qz, q_ts)
            parSocket.send(message.encode())            
            
            in_raw_data = parSocket.recv(1024).decode()
            in_data = str(in_raw_data).split("#")
            flag_cmd = int(in_data[0])
            tau_x = float(in_data[1])
            tau_y = float(in_data[2])
            tau_z = float(in_data[3])            
            throttle = float(in_data[4])
            # print("In: cmd=%d, tx=%f, ty=%f, tz=%f, T=%f" % (flag_cmd, tau_x, tau_y, tau_z, throttle))
            
            if(flag_cmd == 1):
                if(mambo.sensors.flying_state != "landed"):
                    mambo.fly_direct(roll=tau_x, pitch=tau_y, yaw=tau_z, vertical_movement=throttle, duration=0.002)
                    # pass
                elif(mambo.sensors.flying_state == "landed"):
                    mambo.safe_takeoff(4)
                    # pass
            elif(flag_cmd == 0):
                if(mambo.sensors.flying_state != "landed"):
                    mambo.safe_land(4)
                    # pass
            elif(flag_cmd > 1):
                print("Stopping")
                break

        print("Disconnecting")
        mambo.disconnect()
    parSocket.close()

if __name__ == '__main__':
	main()