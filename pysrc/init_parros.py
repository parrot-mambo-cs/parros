#!/usr/bin/env python
# Description:
# Parrot's Mambo ROS server.
# Created by:
# Hilton Tnunay (hilton.tnunay@manchester.ac.uk)

import rospy
import math
import numpy as np
import scipy as sp
import socket
import sys
from geometry_msgs.msg import TransformStamped as geoTrStamp
from geometry_msgs.msg import Pose as geoPose
from geometry_msgs.msg import PoseStamped as geoPoseStamp
from geometry_msgs.msg import Twist as geoTwist
from geometry_msgs.msg import Vector3 as geoVector
from tf.transformations import euler_from_quaternion
from std_msgs.msg import String as stdString
from std_msgs.msg import Float32MultiArray as stdFloat32

NParrot = 2

# update rate of ROS
mambo_idx = 0
update_rate = 100
t_wait = 2.0

parrotPose = [geoPose() for i in range(NParrot)]
parrotInput = [geoTwist() for i in range(NParrot)]

# roboInfoCallback Definition
# def robotInfoCallback_0(poseData):
#     i = 0
#     parrotPose[i] = geoTrStamp_to_Pose(poseData)
#
# def robotInfoCallback_1(poseData):
#     i = 1
#     parrotPose[i] = geoTrStamp_to_Pose(poseData)

# transfer quaternion to euler angle
# def geoTrStamp_to_Pose(poseData):
#     parPose = geoPose()
#     orientation_list = [poseData.transform.rotation.x, poseData.transform.rotation.y, poseData.transform.rotation.z,
#                         poseData.transform.rotation.w]
#     (roll, pitch, yaw) = euler_from_quaternion(orientation_list)
#     parPose.orientation = roll
#     parPose.theta = yaw
#     parPose.theta = yaw
#     parPose.x = poseData.transform.translation.x
#     parPose.y = poseData.transform.translation.y
#     parPose.z = poseData.transform.translation.z
#
#     return robotPose2D

def geoPose_to_Euler(poseData):
    parPose = geoVector()
    orientation_list = [poseData.orientation.x, poseData.orientation.y, poseData.orientation.z, poseData.orientation.w]
    (roll, pitch, yaw) = euler_from_quaternion(orientation_list)
    # parPose.x = roll
    # parPose.y = pitch
    # parPose.z = yaw    
    parPose.x = 57.29578*roll
    parPose.y = 57.29578*pitch
    parPose.z = 57.29578*yaw

    return parPose

def main(args):
    rospy.init_node('parrot_server', anonymous=True)

    # subscribe position from vicon
    # subRobotInfo = rospy.Subscriber("vicon/Mambo0/Mambo0", geoTrStamp, robotInfoCallback_0)
    # subRobotInfo = rospy.Subscriber("vicon/Mambo1/Mambo1", geoTrStamp, robotInfoCallback_1)

    host = "127.0.0.1"
    port = 5000

    parSocket = socket.socket()
    parSocket.bind((host,port))

    parSocket.listen(1)
    conn, addr = parSocket.accept()

    word = "Connection from: " + str(addr)
    rospy.loginfo(word)

    pubBattery = rospy.Publisher("mambo%d/battery" % mambo_idx, stdFloat32, queue_size=1)
    pubStatus = rospy.Publisher("mambo%d/status" % mambo_idx, stdString, queue_size=1)
    pubQuaternion = rospy.Publisher("mambo%d/ori_quaternion" % mambo_idx, geoPose, queue_size=1)
    pubQuaternionStamp = rospy.Publisher("mambo%d/ori_quaternion_stamp" % mambo_idx, geoPoseStamp, queue_size=1)
    pubEuler = rospy.Publisher("mambo%d/ori_euler" % mambo_idx, geoVector, queue_size=1)
    pubTwist = rospy.Publisher("mambo%d/speed" % mambo_idx, geoTwist, queue_size=1)

    battery = stdFloat32()
    status = stdString()
    pose_quat = geoPose()
    pose_quat_stamp = geoPoseStamp()
    pose_eul = geoVector()
    twist = geoTwist()

    rate = rospy.Rate(update_rate)  # ros update rate
    tStart = rospy.get_time()
    while not rospy.is_shutdown():
        t = (rospy.get_time() - tStart)    # time from the program start
        if (rospy.get_time() - tStart) > t_wait:
            data = conn.recv(1024).decode()
            if not data:
                break
            in_data = str(data).split("#")
            pose_quat.position.z = float(in_data[0])
            pose_quat.orientation.w = float(in_data[1])
            pose_quat.orientation.x = float(in_data[2])
            pose_quat.orientation.y = float(in_data[3])
            pose_quat.orientation.z = float(in_data[4])
            pose_eul = geoPose_to_Euler(pose_quat)

            pubQuaternion.publish(pose_quat)
            pubEuler.publish(pose_eul)
            # print ("from connected  user: " + str(in_data[0]) + ", " + str(in_data[1]) + ", " + str(in_data[2]) + ", " + str(in_data[3]) + ", " + str(in_data[4]))

            position = [25.8, 17.4, 3.14]
            out_data = str(position[0]) + "#" + str(position[1]) + "#" + str(position[2])
            # print ("sending: " + out_data)
            conn.send(out_data.encode())
        rate.sleep()

if __name__ == '__main__':
    main(sys.argv)
