#!/usr/bin/env python
# Description:
# Parrot's Mambo ROS server.
# Created by:
# Hilton Tnunay (hilton.tnunay@manchester.ac.uk)

import rospy
import math
import numpy as np
# import scipy as sp
import sys
from geometry_msgs.msg import TransformStamped as geoTrStamp
from geometry_msgs.msg import Pose as geoPose
from geometry_msgs.msg import PoseStamped as geoPoseStamp
from geometry_msgs.msg import Twist as geoTwist
from geometry_msgs.msg import TwistStamped as geoTwistStamp
from geometry_msgs.msg import Vector3 as geoVector
from geometry_msgs.msg import Vector3Stamped as geoVectorStamp
from tf.transformations import euler_from_quaternion
from std_msgs.msg import String as stdString
from std_msgs.msg import Int8 as stdInt8
from std_msgs.msg import Float32 as stdFloat32

n_mambo = 2
update_rate = 100
t_wait = 2.0

s_flying = [stdInt8() for i in range(n_mambo)]
s_command = [geoTwist() for i in range(n_mambo)]

s_battery = [stdFloat32() for i in range(n_mambo)]
s_status = [stdString() for i in range(n_mambo)]
s_velocity = [geoTwistStamp() for i in range(n_mambo)]
s_pose = [geoPoseStamp() for i in range(n_mambo)]
s_euler = [geoVectorStamp() for i in range(n_mambo)]

# def battery_callback0(_battery):
#     i = 0
#     s_battery[i] = _battery

# def battery_callback1(_battery):
#     i = 1
#     s_battery[i] = _battery


# def status_callback0(_status):
#     s_status[0] = _status

# def status_callback1(_status):    
#     s_status[1] = _status


# def velocity_callback0(_velocity):    
#     s_velocity[0] = _velocity

# def velocity_callback1(_velocity):    
#     s_velocity[1] = _velocity


# def pose_callback0(_pose):    
#     s_pose[0] = _pose

# def pose_callback1(_pose):    
#     s_pose[1] = _pose


# def euler_callback0(_euler):    
#     s_euler[0] = _euler

# def euler_callback1(_euler):    
#     s_euler[1] = _euler

def battery_callback(_battery, _idx):
    s_battery[_idx] = _battery

def status_callback(_status, _idx):
    s_status[_idx] = _status

def velocity_callback(_velocity, _idx):
    s_velocity[_idx] = _velocity

def pose_callback(_pose, _idx):
    s_pose[_idx] = _pose

def euler_callback(_euler, _idx):
    s_euler[_idx] = _euler

def main(args):
    rospy.init_node('central', anonymous=True)

    word = "Initialising"
    rospy.loginfo(word)
        
    pubFlying = [rospy.Publisher("mambo%d/flying" % mambo_idx, stdInt8, queue_size=1) for mambo_idx in range(n_mambo)]
    pubCommand = [rospy.Publisher("mambo%d/command" % mambo_idx, geoTwist, queue_size=1) for mambo_idx in range(n_mambo)]

    subBattery = [rospy.Subscriber("mambo%d/battery" % mambo_idx, stdFloat32, battery_callback, (mambo_idx)) for mambo_idx in range(n_mambo)]
    subStatus = [rospy.Subscriber("mambo%d/status" % mambo_idx, stdString, status_callback, (mambo_idx)) for mambo_idx in range(n_mambo)]
    subVelocity = [rospy.Subscriber("mambo%d/velocity" % mambo_idx, geoTwistStamp, velocity_callback, (mambo_idx)) for mambo_idx in range(n_mambo)]
    subPose = [rospy.Subscriber("mambo%d/pose" % mambo_idx, geoPoseStamp, pose_callback, (mambo_idx)) for mambo_idx in range(n_mambo)]
    subEuler = [rospy.Subscriber("mambo%d/euler_angle" % mambo_idx, geoVectorStamp, euler_callback, (mambo_idx)) for mambo_idx in range(n_mambo)]

    p_flying = [stdInt8() for i in range(n_mambo)]
    p_command = [geoTwist() for i in range(n_mambo)]

    rate = rospy.Rate(update_rate)
    tStart = rospy.get_time()
    while not rospy.is_shutdown():
        t = (rospy.get_time() - tStart)
        if(t > t_wait):
            for i in range(n_mambo):
                word = "Info%d: b%0.02f s.%s vx%0.02f vy%0.02f vz%0.02f alt%0.02f qw%0.02f qx%0.02f qy%0.02f qz%0.02f" % (i, s_battery[i].data, s_status[i].data, s_velocity[i].twist.linear.x, s_velocity[i].twist.linear.y, s_velocity[i].twist.linear.z, s_pose[i].pose.position.z, s_pose[i].pose.orientation.w, s_pose[i].pose.orientation.x, s_pose[i].pose.orientation.y, s_pose[i].pose.orientation.z)
                rospy.loginfo(word)
            
            if(t<=10.0):
                for i in range(n_mambo):
                    p_flying[i].data = 0
                    p_command[i].angular.x = 0
                    p_command[i].angular.y = 0
                    p_command[i].angular.z = 0
                    p_command[i].linear.z = 0
            elif(t>10.0 and t<=25.0):
                for i in range(n_mambo):
                    p_flying[i].data = 1
                    p_command[i].angular.x = 0
                    p_command[i].angular.y = 0
                    p_command[i].angular.z = 15
                    p_command[i].linear.z = 0
            elif(t>25.0 and t<=30.0):
                for i in range(n_mambo):
                    p_flying[i].data = 0
                    p_command[i].angular.x = 0
                    p_command[i].angular.y = 0
                    p_command[i].angular.z = 10
                    p_command[i].linear.z = 0
            elif(t>30.0):
                for i in range(n_mambo):
                    p_flying[i].data = 2
                    p_command[i].angular.x = 0
                    p_command[i].angular.y = 0
                    p_command[i].angular.z = 0
                    p_command[i].linear.z = 0

            for i in range(n_mambo):
                pubFlying[i].publish(p_flying[i])
                pubCommand[i].publish(p_command[i])
            
        rate.sleep()

if __name__ == '__main__':
    main(sys.argv)
