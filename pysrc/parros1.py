#!/usr/bin/env python
# Description:
# Parrot's Mambo ROS server.
# Created by:
# Hilton Tnunay (hilton.tnunay@manchester.ac.uk)

import rospy
import math
import numpy as np
# import scipy as sp
import socket
import sys
from geometry_msgs.msg import TransformStamped as geoTrStamp
from geometry_msgs.msg import Pose as geoPose
from geometry_msgs.msg import PoseStamped as geoPoseStamp
from geometry_msgs.msg import Twist as geoTwist
from geometry_msgs.msg import TwistStamped as geoTwistStamp
from geometry_msgs.msg import Vector3 as geoVector
from geometry_msgs.msg import Vector3Stamped as geoVectorStamp
# from tf.transformations import euler_from_quaternion
from std_msgs.msg import String as stdString
from std_msgs.msg import Int8 as stdInt8
from std_msgs.msg import Float32 as stdFloat32

# update rate of ROS
mambo_idx = 1
update_rate = 100
t_wait = 2.0

s_flying = stdInt8()
s_command = geoTwist()

# roboInfoCallback Definition
# def robotInfoCallback_0(poseData):
#     i = 0
#     parrotPose[i] = geoTrStamp_to_Pose(poseData)

def flying_callback(_fly):
    global s_flying
    s_flying = _fly

def command_callback(_cmd):
    global s_command
    s_command = _cmd

def euler_from_quaternion(_orientation_list):    
    x = _orientation_list[0]
    y = _orientation_list[1]
    z = _orientation_list[2]
    w = _orientation_list[3]
    ysqr = y * y

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + ysqr)    
    X = math.atan2(t0, t1)

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    Y = math.asin(t2)

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (ysqr + z * z)
    Z = math.atan2(t3, t4)

    return X, Y, Z

def geoPose_to_Euler(poseDataStamp):
    poseData = poseDataStamp.pose
    parPose = geoVector()

    orientation_list = [poseData.orientation.x, poseData.orientation.y, poseData.orientation.z, poseData.orientation.w]
    (roll, pitch, yaw) = euler_from_quaternion(orientation_list)    
    # parPose.x = roll
    # parPose.y = pitch
    # parPose.z = yaw    
    parPose.x = 57.29578*roll
    parPose.y = 57.29578*pitch
    parPose.z = 57.29578*yaw

    return parPose

def parse_from_parrot(_data, _time_stamp):
    m_battery = stdFloat32()
    m_status = stdString()
    m_velocity = geoTwistStamp()
    m_pose = geoPoseStamp()
    m_euler = geoVectorStamp()

    in_data = str(_data).split("#")

    m_battery.data = float(in_data[0])

    m_status.data = str(in_data[1])

    m_velocity.header.stamp = _time_stamp
    m_velocity.twist.linear.x = float(in_data[2])
    m_velocity.twist.linear.y = float(in_data[3])
    m_velocity.twist.linear.z = float(in_data[4])

    m_pose.header.stamp = _time_stamp
    m_pose.pose.position.z = float(in_data[5])
    m_pose.pose.orientation.w = float(in_data[6])
    m_pose.pose.orientation.x = float(in_data[7])
    m_pose.pose.orientation.y = float(in_data[8])
    m_pose.pose.orientation.z = float(in_data[9])
    
    m_euler.header.stamp = _time_stamp
    m_euler.vector = geoPose_to_Euler(m_pose)
    
    return (m_battery, m_status, m_velocity, m_pose, m_euler)

def main(args):
    rospy.init_node('parros1', anonymous=True)    

    host = "127.0.0.1"
    port = 5000

    parSocket = socket.socket()
    parSocket.bind((host,port))

    parSocket.listen(1)
    conn, addr = parSocket.accept()

    word = "Connection from: " + str(addr)
    rospy.loginfo(word)
    
    # subVicon = rospy.Subscriber("vicon/Mambo0/Mambo0", geoTrStamp, viconCallback)
    subFlying = rospy.Subscriber("mambo%d/flying" % mambo_idx, stdInt8, flying_callback)
    subCommand = rospy.Subscriber("mambo%d/command" % mambo_idx, geoTwist, command_callback)

    pubBattery = rospy.Publisher("mambo%d/battery" % mambo_idx, stdFloat32, queue_size=1)
    pubStatus = rospy.Publisher("mambo%d/status" % mambo_idx, stdString, queue_size=1)
    pubVelocity = rospy.Publisher("mambo%d/velocity" % mambo_idx, geoTwistStamp, queue_size=1)
    pubPose = rospy.Publisher("mambo%d/pose" % mambo_idx, geoPoseStamp, queue_size=1)
    pubEuler = rospy.Publisher("mambo%d/euler_angle" % mambo_idx, geoVectorStamp, queue_size=1) 

    p_battery = stdFloat32()
    p_status = stdString()
    p_velocity = geoTwistStamp()
    p_pose = geoPoseStamp()
    p_euler = geoVectorStamp()   

    rate = rospy.Rate(update_rate)
    tStart = rospy.get_time()
    while not rospy.is_shutdown():
        t = (rospy.get_time() - tStart)
        if t > t_wait:
            data = conn.recv(1024).decode()
            if not data:
                break            
            
            (m_battery, m_status, m_velocity, m_pose, m_euler) = parse_from_parrot(data, rospy.get_rostime())
            # print("Battery: %f" % m_battery.data)

            pubBattery.publish(m_battery)
            pubStatus.publish(m_status)
            pubVelocity.publish(m_velocity)
            pubPose.publish(m_pose)
            pubEuler.publish(m_euler)

            out_data = str(s_flying.data) + "#" + str(s_command.angular.x) + "#" + str(s_command.angular.y) + "#" + str(s_command.angular.z) + "#" + str(s_command.linear.z)
            conn.send(out_data.encode())
        rate.sleep()

if __name__ == '__main__':
    main(sys.argv)
