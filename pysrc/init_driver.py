#!/usr/bin/env python3
import socket
import time
from pyparrot.Minidrone import Mambo

mamboAddr = "F0:03:8C:2F:5E:4D"
mambo = Mambo(mamboAddr, use_wifi=True)

# def Quat_to_Euler(w,x,y,z):    
#     orientation_list = [x,y,z,w]
#     (roll, pitch, yaw) = euler_from_quaternion(orientation_list)
#     roll = 57.29578*roll
#     pitch = 57.29578*pitch
#     yaw = 57.29578*yaw
#     return roll, pitch, yaw

def main():
    print("Trying to connect")
    success = mambo.connect(num_retries=3)
    print("Connected: %s" % success)

    if (success):
        host = '127.0.0.1'
        port = 5000
        
        parSocket = socket.socket()
        parSocket.connect((host,port))

        print("Loading params")
        mambo.smart_sleep(2)
        mambo.ask_for_state_update()
        mambo.smart_sleep(2)

        print("Taking off!")
        mambo.safe_takeoff(3)

        t_start = time.time()
        ts_iter = time.time()
                
        while(mambo.sensors.flying_state != "emergency"):
            mambo.ask_for_state_update()
            t_delta = time.time() - t_start
            dt_iter = time.time() - ts_iter        

            # if(dt_iter >= 0.01):
            batt = mambo.sensors.battery
            state = mambo.sensors.flying_state
            vx = mambo.sensors.speed_x
            vy = mambo.sensors.speed_y
            vz = mambo.sensors.speed_z
            v_ts = mambo.sensors.speed_ts
            alt = mambo.sensors.altitude
            alt_ts = mambo.sensors.altitude_ts
            qw = mambo.sensors.quaternion_w
            qx = mambo.sensors.quaternion_x
            qy = mambo.sensors.quaternion_y
            qz = mambo.sensors.quaternion_z
            q_ts = mambo.sensors.quaternion_ts
            roll, pitch, yaw = mambo.sensors.quaternion_to_euler_angle(qw, qx, qy, qz)
            # roll, pitch, yaw = Quat_to_Euler(qw, qx, qy, qz)

            print("Time: %f" % t_delta)
            print("Battery: %f" % batt)
            print("Flying state: %s" % state)
            print("Velocity: x=%f, y=%f, z=%f, ts=%f" % (vx, vy, vz, v_ts))
            print("Altitude: h=%f, ts=%f" % (alt, alt_ts))
            print("Quaternion: w=%f, x=%f, y=%f, z=%f, ts=%f" % (qw, qx, qy, qz, q_ts))
            print("Euler: r=%f, p=%f, y=%f" % (roll, pitch, yaw))
            print("=================================")

            message = "%f#%f#%f#%f#%f" % (alt, qw, qx, qy, qz)
            # message = "%f#%f#%f#%f#%f#%f#%f" % (alt, alt_ts, qw, qx, qy, qz, q_ts)
            parSocket.send(message.encode())            
            
            in_raw_data = parSocket.recv(1024).decode()
            in_data = str(in_raw_data).split("#")
            print ('Position: x=%s, y=%s, z=%s' % (in_data[0], in_data[1], in_data[2]))
            ts_iter = time.time()         
            
            if(t_delta >= 24.0):
                print("Landing")            
                mambo.safe_land(3)
                print("Flying state is %s" % mambo.sensors.flying_state)
                break
            elif(t_delta >= 4.0 and t_delta <= 12.0):
                print("Yawing positive")
                mambo.fly_direct(roll=0, pitch=0, yaw=10, vertical_movement=0, duration=0.002)
                # mambo.fly_direct(roll=0, pitch=0, yaw=50, vertical_movement=0)
            elif(t_delta > 12.0 and t_delta <= 20.0):
                print("Yawing negative")
                mambo.fly_direct(roll=0, pitch=0, yaw=-10, vertical_movement=0, duration=0.002)
                # mambo.fly_direct(roll=0, pitch=0, yaw=-50, vertical_movement=0)
            # t_start = time.time()

        print("disconnect")
        mambo.disconnect()
    parSocket.close()

if __name__ == '__main__':
	main()