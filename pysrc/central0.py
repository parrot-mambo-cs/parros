#!/usr/bin/env python
# Description:
# Parrot's Mambo ROS server.
# Created by:
# Hilton Tnunay (hilton.tnunay@manchester.ac.uk)

import rospy
import math
import numpy as np
# import scipy as sp
import sys
from geometry_msgs.msg import TransformStamped as geoTrStamp
from geometry_msgs.msg import Pose as geoPose
from geometry_msgs.msg import PoseStamped as geoPoseStamp
from geometry_msgs.msg import Twist as geoTwist
from geometry_msgs.msg import TwistStamped as geoTwistStamp
from geometry_msgs.msg import Vector3 as geoVector
from geometry_msgs.msg import Vector3Stamped as geoVectorStamp
from tf.transformations import euler_from_quaternion
from std_msgs.msg import String as stdString
from std_msgs.msg import Int8 as stdInt8
from std_msgs.msg import Float32 as stdFloat32

# update rate of ROS
mambo_idx = 0
update_rate = 100
t_wait = 2.0

s_flying = stdInt8()
s_command = geoTwist()

s_battery = stdFloat32()
s_status = stdString()
s_velocity = geoTwistStamp()
s_pose = geoPoseStamp()
s_euler = geoVectorStamp()

def battery_callback(_battery):
    global s_battery
    s_battery = _battery

def status_callback(_status):
    global s_status
    s_status = _status

def velocity_callback(_velocity):
    global s_velocity
    s_velocity = _velocity

def pose_callback(_pose):
    global s_pose
    s_pose = _pose

def euler_callback(_euler):
    global s_euler
    s_euler = _euler

def main(args):
    rospy.init_node('central0', anonymous=True)

    word = "Initialising"
    rospy.loginfo(word)
        
    pubFlying = rospy.Publisher("mambo%d/flying" % mambo_idx, stdInt8, queue_size=1)
    pubCommand = rospy.Publisher("mambo%d/command" % mambo_idx, geoTwist, queue_size=1)

    subBattery = rospy.Subscriber("mambo%d/battery" % mambo_idx, stdFloat32, battery_callback)
    subStatus = rospy.Subscriber("mambo%d/status" % mambo_idx, stdString, status_callback)
    subVelocity = rospy.Subscriber("mambo%d/velocity" % mambo_idx, geoTwistStamp, velocity_callback)
    subPose = rospy.Subscriber("mambo%d/pose" % mambo_idx, geoPoseStamp, pose_callback)
    subEuler = rospy.Subscriber("mambo%d/euler_angle" % mambo_idx, geoVectorStamp, euler_callback)

    p_flying = stdInt8()
    p_command = geoTwist()

    rate = rospy.Rate(update_rate)
    tStart = rospy.get_time()
    while not rospy.is_shutdown():
        t = (rospy.get_time() - tStart)
        if(t > t_wait):
            word = "Info: b%0.02f s.%s vx%0.02f vy%0.02f vz%0.02f alt%0.02f qw%0.02f qx%0.02f qy%0.02f qz%0.02f" % (s_battery.data, s_status.data, s_velocity.twist.linear.x, s_velocity.twist.linear.y, s_velocity.twist.linear.z, s_pose.pose.position.z, s_pose.pose.orientation.w, s_pose.pose.orientation.x, s_pose.pose.orientation.y, s_pose.pose.orientation.z)
            rospy.loginfo(word)
            
            if(t<=10.0):
                p_flying.data = 0
                p_command.angular.x = 0
                p_command.angular.y = 0
                p_command.angular.z = 0
                p_command.linear.z = 0
            elif(t>10.0 and t<=25.0):
                p_flying.data = 1
                p_command.angular.x = 0
                p_command.angular.y = 0
                p_command.angular.z = 10
                p_command.linear.z = 0
            elif(t>25.0 and t<=30.0):
                p_flying.data = 0
                p_command.angular.x = 0
                p_command.angular.y = 0
                p_command.angular.z = 10
                p_command.linear.z = 0
            elif(t>30.0):
                p_flying.data = 2
                p_command.angular.x = 0
                p_command.angular.y = 0
                p_command.angular.z = 0
                p_command.linear.z = 0

            pubFlying.publish(p_flying)
            pubCommand.publish(p_command)            
            
        rate.sleep()

if __name__ == '__main__':
    main(sys.argv)
